const expectChai = require('chai').expect


class documentationPage
{
    get developerGuideLocator() {return $("//span[@title='Developer Guide']")};
    get compTabLocator() {return $("//span[@title='Component Reference']")};
    get comTabLocator(){return $('/html/body/div[5]/div[1]/componentreference-header/header/div/componentreference-nav-bar/div/nav/div/componentreference-nav-item[1]/a/span')}
    //
    verifyFirstPage()
    {
        var page1 =this.developerGuideLocator;
        page1.waitForDisplayed(10000);
        var page1text = page1.getText();
        expectChai(page1text).to.equal("Developer Guide");
    }

    clickOnTabComp()
    {   
        var tab = this.compTabLocator;
        tab.waitForDisplayed(10000);
        tab.click();
        expect(browser).toHaveUrlContaining('components');
    }
}

module.exports = new documentationPage();