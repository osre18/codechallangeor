const expectChai = require('chai').expect;

class componentPage
{
  get searchLocator() {return $('//input[@type="search"]')};

  get dataTableButtonLocator() {return $('//span[@title="datatable"]')};

  get exampleDropDownLocator() {return $("//input[@name='example']")};

  get dropDownOptionLocator () {return $('//lightning-base-combobox-item[@data-value="withInlineEdit"]')};

  get runButtonLocator() { return $('/html/body/div[4]/div[1]/div[2]/div/div/div/div/div[1]/componentreference-playground-example/div/div[3]/lightning-button-group/slot/lightning-button/button')};

  get tableLocator(){return $('/html/body/main/c-withinlineedit/div/lightning-datatable/div[2]/div/div/div/table/tbody/tr')};

  get labelHoverOverLocator() { return $("/html/body/main/c-withinlineedit/div/lightning-datatable/div[2]/div/div/div/table/tbody/tr[3]/th")};

  get labelEditButtonLocator(){return $('//tbody/tr[3]/th[1]/lightning-primitive-cell-factory[1]/span[1]/button[1]/lightning-primitive-icon[1]')};

  get labelTextBoxLocator(){return $('//input[@type="text"]')};

  get websideHoverOverLocator() { return $("/html/body/main/c-withinlineedit/div/lightning-datatable/div[2]/div/div/div/table/tbody/tr[3]/td[2]")};

  get websideEditButtonLocator() { return $('//tbody/tr[3]/td[3]/lightning-primitive-cell-factory[1]/span[1]/button[1]/lightning-primitive-icon[1]')};

  get websideTextBoxLocator() { return $('//input[@type="url"]')};
  
  get phoneHoverOverLocator(){return $("/html/body/main/c-withinlineedit/div/lightning-datatable/div[2]/div/div/div/table/tbody/tr[3]/td[3]")};

  get phoneEditButtonLocator(){return $('//tbody/tr[3]/td[4]/lightning-primitive-cell-factory[1]/span[1]/button[1]/lightning-primitive-icon[1]')};

  get phoneTextBoxLocator(){return $('//input[@type="tel"]')};

  get closeHoverOverLocator(){return $("/html/body/main/c-withinlineedit/div/lightning-datatable/div[2]/div/div/div/table/tbody/tr[3]/td[4]")};

  get closeEditButtonLocator(){return $('//tbody/tr[3]/td[5]/lightning-primitive-cell-factory[1]/span[1]/button[1]/lightning-primitive-icon[1]')};
  
  get closeDatePicketLocator(){return $('//input[@type="text"]')};

  get closeTodayLinkLocator(){return $("//button[normalize-space()='Today']")};

  get closeTimeLocato(){return $('/html/body/main/c-withinlineedit/div/lightning-datatable/div[2]/lightning-primitive-datatable-iedit-panel/section/div[1]/form/lightning-primitive-datatable-iedit-input-wrapper/slot/lightning-primitive-datatable-iedit-type-factory/lightning-input/lightning-datetimepicker/div/fieldset/div/div/div/lightning-timepicker/div/lightning-base-combobox/div/div[1]/input')};

  get balanceHoveroverLocator(){return $("/html/body/main/c-withinlineedit/div/lightning-datatable/div[2]/div/div/div/table/tbody/tr[3]/td[5]")};

  get balanceEditButtonLocator(){return $('//tbody/tr[3]/td[6]/lightning-primitive-cell-factory[1]/span[1]/button[1]/lightning-primitive-icon[1]')};

  get balanceTextBoxLocator(){return $('//input[@type="text"]')};

  get iFrameLocator(){return $('//iframe[@name="preview"]')};

  get webValidationLocator(){return $('/html/body/main/c-withinlineedit/div/lightning-datatable/div[2]/div/div/div/table/tbody/tr[3]/td[3]/lightning-primitive-cell-factory/span')}

  get phoneValidationLocator(){return $('/html/body/main/c-withinlineedit/div/lightning-datatable/div[2]/div/div/div/table/tbody/tr[3]/td[4]/lightning-primitive-cell-factory/span')}

  get balanceValidationLocator(){return $('/html/body/main/c-withinlineedit/div/lightning-datatable/div[2]/div/div/div/table/tbody/tr[3]/td[6]')}

  get auxLocator(){return $('/html/body/main/c-withinlineedit/div/lightning-datatable/div[2]/div/div/div/table/tbody/tr[1]/td[5]/lightning-primitive-cell-factory/span')}

  get closeAtValidationLocator(){return $('/html/body/main/c-withinlineedit/div/lightning-datatable/div[2]/div/div/div/table/tbody/tr[3]/td[5]/lightning-primitive-cell-factory/span')}

  
  selectDropDown(option)
  {
    var dropDown = this.exampleDropDownLocator
    browser.pause(2000)
    dropDown.click();
    var dorpOp =$('//lightning-base-combobox-item[@data-value="'+option+'"]');
    dorpOp.waitForClickable({timeout:10000});
    dorpOp.click();
    this.runButtonLocator.click();
    browser.pause(12000);
  }

  searchBar(valueToSearch)
  {
    var flag = false;
    while(flag == false){
      try{
        flag = true;
        this.searchLocator.waitForExist({timeout:1000});
        this.searchLocator.setValue(valueToSearch);
        $('//span[@title="'+valueToSearch+'"]').click();
      }
      catch{
        flag = false;
        console.log('can not find the tab');
        browser.refresh();
      }
    }
  }
  
  editData(label, website, phone, closeAt, balance)
  {
    //go inside first frame
    var frame = this.iFrameLocator;
    frame.waitForExist({timeout: 10000});
    browser.switchToFrame(frame);

    //go inside second frame
    frame = this.iFrameLocator;
    frame.waitForExist({timeout: 10000});
    browser.switchToFrame(frame);

    //edit label column
    this.labelHoverOverLocator.moveTo();
    this.labelEditButtonLocator.click();
    this.labelTextBoxLocator.setValue(label);

    // edit website column
    this.websideHoverOverLocator.moveTo();
    this.websideEditButtonLocator.click();
    this.websideTextBoxLocator.setValue(website);

    // edit phone column
    this.phoneHoverOverLocator.moveTo();
    this.phoneEditButtonLocator.click();
    this.phoneTextBoxLocator.setValue(phone);

    // edit closeAt column
    this.closeHoverOverLocator.moveTo();
    this.closeEditButtonLocator.click();
    this.closeDatePicketLocator.click();
    this.closeTodayLinkLocator.click();
    this.closeTimeLocato.clearValue();
    this.closeTimeLocato.setValue(closeAt);
    this.auxLocator.click();

    // edit balance column
    this.balanceHoveroverLocator.moveTo();
    this.balanceEditButtonLocator.click();
    this.balanceTextBoxLocator.setValue(balance);

    //aux to save the data on balance 
    this.auxLocator.click();

    browser.switchToFrame(null);
  }

  dataVerification(label, website, phone, closeAt, balance)
  {
    //go inside first frame
    var frame = this.iFrameLocator;
    frame.waitForExist({timeout:10000});
    browser.switchToFrame(frame);

    //go inside second frame
    frame = this.iFrameLocator;
    frame.waitForExist({timeout:10000});
    browser.switchToFrame(frame);

    //verify data on label column
    var labelText = this.labelHoverOverLocator;
    labelText.moveTo();
    var actualLabel = labelText.getText();
    console.log(actualLabel);
    expectChai(actualLabel).to.contain(label);

    //verify website on label column
    var webText = this.webValidationLocator;
    webText.moveTo();
    var actualWeb = webText.getText();
    console.log(actualWeb);
    expectChai(actualWeb).to.contain(website);

    //verify phone on label column
    var phoneNumber = this.phoneValidationLocator;
    phoneNumber.moveTo();
    var actualPhone = phoneNumber.getText();
    console.log(actualPhone);
    expectChai(actualPhone).to.contain(phone);

    //verify close At 
    var today = new Date();
    var dd = String(today.getDate());
    console.log(dd);
    var closeDate = this.closeAtValidationLocator.getText();
    expectChai(closeDate).to.contain(dd);

    //verify balance
    var balanceNumber = this.balanceValidationLocator;
    balanceNumber.moveTo();
    var actualBalance = balanceNumber.getText();
    console.log(actualBalance);
    expectChai(actualBalance).to.contain(balance);

    browser.switchToFrame(null);

  }
}

module.exports = new componentPage();