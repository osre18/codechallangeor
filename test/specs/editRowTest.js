const componentPage = require('../pageobjects/componentPage');
const documentPage = require('../pageobjects/documentationPage');
const expectChai = require('chai').expect;


describe('Test Code challange', ()=>{   

    it('Open the browse and verify that is correctly displayed', ()=>{
        browser.url("docs/component-library/documentation/en/48.0/lwc");
        documentPage.verifyFirstPage();
    })

    it('Click on the components tab and verify that is the correct page', ()=>{
        documentPage.clickOnTabComp();
    })
    
    it('Search for datatable in the search box and click on the result', ()=>{
        componentPage.searchBar('datatable');
    })

    it('Select with Inline Edit from the dropdown and click on run button', ()=>{
        componentPage.selectDropDown('withInlineEdit');
    })

    it('Edit the columns in the row 3', ()=>{
        componentPage.editData('Larry Page', 'https://google.com', '(555)-755-6575', '12:57 PM', '70.54');
    })

    it('Verify that the data entered is displayed correctly', ()=>{
        componentPage.dataVerification('Larry Page', 'https://google.com', '(555)-755-6575', '12:57 PM', '70.54');
    })
        
})